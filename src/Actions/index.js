import actionNames from './types'

export const addFavorite = (value) => ({
  type: actionNames.ADDFAV,
  payload: value,
})

export const deleteFavorite = (value) => ({
  type: actionNames.DELETEFAV,
  payload: value,
})

export const deleteTag = (favorite, tag) => ({
  type: actionNames.DELETETAG,
  payload: { favorite, tag },
})

export const addFilter = (value) => ({
  type: actionNames.ADDFILTER,
  payload: value,
})
