const actionNames = {
  ADDFAV: 'ADD_FAVORITE',
  DELETEFAV: 'DELETE_FAVORITE',
  DELETETAG: 'DELETE_TAG',
  ADDFILTER: 'ADD_FILTER',
}

export default actionNames
