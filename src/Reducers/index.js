import { combineReducers } from 'redux'
import favoritesReducer from './favoritesReducer'
import filterReducer from './filterReducer'

const Reducers = combineReducers({
  favoritesState: favoritesReducer,
  filterState: filterReducer,
})

export default Reducers
