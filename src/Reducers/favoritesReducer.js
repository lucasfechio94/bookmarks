import actionNames from '../Actions/types'

const initialState = {
  favorites: [
    { title: 'Crédito', link: 'www.bebluecredito.com.br', tags: ['crédito'] },
    { title: 'Wallet', link: 'www.beblue.com.br/wallet', tags: ['wallet'] },
    { title: 'Credenciamento', link: 'www.beblue.com.br/credenciamento', tags: ['wallet'] },
    { title: 'Seu dinheiro de volta', link: 'www.beblue.com.br', tags: ['beblue', 'cashback', 'saldo'] },
  ],
}

const favoritesReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionNames.ADDFAV: {
      return { ...state, favorites: [...state.favorites, action.payload] }
    }
    case actionNames.DELETEFAV: {
      const newFavorites = [
        ...state.favorites.slice(0, action.payload), ...state.favorites.slice(action.payload + 1),
      ]
      return { ...state, favorites: newFavorites }
    }
    case actionNames.DELETETAG: {
      const newFavorites = [...state.favorites]
      newFavorites[action.payload.favorite].tags.splice(action.payload.tag, 1)
      return { ...state, favorites: newFavorites }
    }
    default:
      return state
  }
}

export default favoritesReducer
