import actionNames from '../Actions/types'

const initialState = {
  filter: '',
}

const filterReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionNames.ADDFILTER:
      return { ...state, filter: action.payload }
    default:
      return state
  }
}

export default filterReducer
