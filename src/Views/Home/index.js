import React from 'react'
import injectSheet from 'react-jss'
import Form from '../../Components/Form'
import FavoriteList from '../../Components/FavoriteList'

const styles = {
  container: {
    width: 'auto',
    height: 'auto',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '5%',
  },
  box: {
    padding: 25,
    width: '70%',
    background: '#fff',
  },
}

const Home = injectSheet(styles)((props) => {
  const { classes } = props
  return (
    <div className={classes.container}>
      <div className={classes.box}>
        <Form />
        <FavoriteList />
      </div>
    </div>

  )
})


export default Home
