import React from 'react'
import { connect } from 'react-redux'
import Favorite from '../Favorite'

const FavoriteList = (props) => (
  props.favorites.map((elem) => (
    props.filter === ''
      ? (
        <Favorite
          key={elem}
          favorite={elem}
        />
      ) : (elem.tags.some((tag) => tag.includes(props.filter))
        && (
          <Favorite
            key={elem}
            favorite={elem}
          />
        )
      )
  ))
)

const mapStateToProps = (store) => ({
  favorites: store.favoritesState.favorites,
  filter: store.filterState.filter,
})

export default connect(mapStateToProps)(FavoriteList)
