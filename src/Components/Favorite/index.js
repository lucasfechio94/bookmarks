import React from 'react'
import { connect } from 'react-redux'
import injectSheet from 'react-jss'
import { deleteFavorite } from '../../Actions'
import Tag from '../Tag'
import trash from '../../../Assets/Trash.png'

const styles = {
  favorite: {
    borderBottom: '1px solid gray',
    display: 'grid',
    gridTemplateColumns: '70% 30%',
    padding: 15,
    fontFamily: 'Arial, Helvetica, sans-serif',
  },
  delete: {
    padding: 50,
    display: 'inline',
    cursor: 'pointer',
    textAlign: ' right',
    color: 'rgb(197, 203, 207)',
  },
  deleteText: {
    display: 'inline',
    marginLeft: 10,
    '@media (max-width: 1024px)': {
      display: 'none',
    },
  },
  fields: {
    padding: 10,
    display: 'grid',
  },
  title: {
    fontSize: 20,
    color: 'rgb(71, 82, 93)',
    paddingBottom: 20,
    '@media (max-width: 700px)': {
      fontSize: '100%',
    },
  },
  link: {
    textDecoration: 'none',
    color: 'rgb(32, 155, 255)',
    '@media (max-width: 700px)': {
      fontSize: '70%',
    },
  },
  tags: {
    paddingTop: 20,
  },
}

const Favorite = injectSheet(styles)((props) => {
  const { classes } = props
  return (
    <div className={classes.favorite}>
      <div className={classes.fields}>
        <div className={classes.title}>
          {props.favorite.title}
        </div>
        <div>
          <a className={classes.link} href={`https://${props.favorite.link}`}>{props.favorite.link}</a>
        </div>
        <div className={classes.tags}>
          {
            props.favorite.tags.map((elem, id) => elem !== '' && <Tag key={elem} id={id} favorite={props.favorites.indexOf(props.favorite)} tag={elem} />)
          }
        </div>
      </div>

      <div role='button' tabIndex={0} className={classes.delete} onClick={() => props.deleteFavorite(props.favorites.indexOf(props.favorite))}>
        <img src={trash} alt='' />
        <span className={classes.deleteText}>Delete</span>
      </div>
    </div>
  )
})

const mapStateToProps = (store) => ({
  favorites: store.favoritesState.favorites,
})

export default connect(mapStateToProps, { deleteFavorite })(Favorite)
