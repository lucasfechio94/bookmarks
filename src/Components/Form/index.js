import React, { useState } from 'react'
import { connect } from 'react-redux'
import { uniq } from 'lodash'
import injectSheet from 'react-jss'
import { addFavorite, addFilter } from '../../Actions'
import addBlue from '../../../Assets/Adicionar_azul.png'
import addGray from '../../../Assets/Adicionar_cinza.png'
import searchBlue from '../../../Assets/Lupa_azul.png'
import searchGray from '../../../Assets/Lupa_cinza.png'

const styles = {
  form: {
    display: 'grid',
    gridTemplateColumns: '10% 90%',
    justifyContent: 'center',
    '@media (max-width: 700px)': {
      gridTemplateColumns: '20% 80%',
    },
  },
  buttons: {
    display: 'grid',
    gridTemplateColumns: '50% 50%',
    cursor: 'pointer',
    justifyContent: 'center',
  },
  insertInputs: {
    display: 'grid',
    gridTemplateColumns: '33% 33% 33%',
    justifyContent: 'center',
  },
  fields: {
    maxWidth: '100%',
    borderRadius: 5,
    padding: 5,
    marginLeft: 10,
  },
  images: {
    width: '25px',
  },
}

const Form = injectSheet(styles)((props) => {
  const { classes } = props
  const [title, setTitle] = useState('')
  const [link, setLink] = useState('')
  const [tags, setTags] = useState('')
  const [mode, setMode] = useState('insert')

  const submit = (event) => {
    event.preventDefault()
    if (title === '' || link === '' || tags === '') {
      // TODO: melhorar feedback
      alert('Preencha todos os campos')
    } else {
      const favorite = {
        title,
        link,
        tags: uniq(tags.trim().split(/\s+/)),
      }
      props.addFavorite(favorite)
      setTitle('')
      setLink('')
      setTags('')
    }
  }

  if (mode === 'insert') {
    props.addFilter('')
    return (
      <div className={classes.form}>
        <div className={classes.buttons}>
          <img className={classes.images} src={searchGray} onClick={() => setMode('filter')} alt='' />
          <img className={classes.images} src={addBlue} onClick={() => setMode('insert')} alt='' />
        </div>
        <form className={classes.insertInputs} onSubmit={(event) => submit(event)}>
          <input type='text' className={classes.fields} value={title} placeholder='Title' onChange={(event) => setTitle(event.target.value)} />
          <input type='text' className={classes.fields} value={link} placeholder='Link' onChange={(event) => setLink(event.target.value)} />
          <input type='text' className={classes.fields} value={tags} placeholder='Tags' onChange={(event) => setTags(event.target.value)} />
          <button type='submit' hidden />
        </form>
      </div>
    )
  }
  return (
    <div className={classes.form}>
      <div className={classes.buttons}>
        <img className={classes.images} src={addGray} onClick={() => setMode('insert')} alt='' />
        <img className={classes.images} src={searchBlue} onClick={() => setMode('filter')} alt='' />
      </div>
      <input type='text' className={classes.fields} placeholder='Filter by Tag' onChange={(event) => props.addFilter(event.target.value)} />
    </div>
  )
})


export default connect(null, { addFavorite, addFilter })(Form)
