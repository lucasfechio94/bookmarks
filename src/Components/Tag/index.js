import React from 'react'
import { connect } from 'react-redux'
import injectSheet from 'react-jss'
import { deleteTag } from '../../Actions'
import deleteImg from '../../../Assets/X.png'

const styles = {
  tag: {
    minWidth: '80px',
    height: '20px',
    position: 'relative',
    background: 'rgb(30, 104, 193)',
    cursor: 'pointer',
    float: 'left',
    padding: 5,
    color: 'white',
    borderRadius: 5,
    margin: 3,
  },
  delete: {
    marginLeft: 30,
    cursor: 'pointer',
  },
}

const Tag = injectSheet(styles)((props) => {
  const { classes } = props
  return (
    <div className={classes.tag}>
      <span className={classes.text}>{props.tag}</span>
      <img
        className={classes.delete}
        onClick={() => props.deleteTag(props.favorite, props.id)}
        src={deleteImg}
        alt=''
      />
    </div>
  )
})

export default connect(null, { deleteTag })(Tag)
