### Installing

Clone this repo and install the project's dependencies by running:

```
npm install
```

## Run

To start server, run:

```
npm start
```
